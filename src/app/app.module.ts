import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { environment } from '../environments/environment';

import Backendless from 'backendless';
import { USER_REPOSITORY_REMOTE } from './usecase/repository/User/UserRepositoryRemote';
import { CloudUserRepository } from './remote/cloud/CloudUserRepository';

Backendless.initApp(environment.backendless.APP_ID, environment.backendless.API_KEY);
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: USER_REPOSITORY_REMOTE, useClass: CloudUserRepository }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
