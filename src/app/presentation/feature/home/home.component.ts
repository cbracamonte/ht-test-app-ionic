import { OnInit, Component } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/entities/User';


@Component({
    selector: 'app-ht-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    public user: User;
    constructor(private router: Router) {
        const navigation = this.router.getCurrentNavigation();
        const state = navigation.extras.state as User;
        this.user = new User();
        this.user.name = state.name;
    }

    ngOnInit(): void {

    }
}
