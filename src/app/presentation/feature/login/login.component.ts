
import { OnInit, Component, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, NavigationExtras } from '@angular/router';
import { User } from 'src/app/entities/User';
import { LoginView, LoginPresenter } from './login.presenter';
import { LoadingController, ToastController } from '@ionic/angular';

@Component({
    selector: 'app-ht-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})


export class LoginComponent implements OnInit, OnDestroy, LoginView {
    public formLogin: FormGroup;
    private user: User;
    private loginPresenter: LoginPresenter;
    constructor(
        private formBuilder: FormBuilder,
        private loadingController: LoadingController,
        private toastController: ToastController,
        private router: Router,
        loginPresenter: LoginPresenter) {
        this.user = new User();
        this.loginPresenter = loginPresenter;
        this.loginPresenter.setLoginView(this);
    }


    public ngOnInit(): void {
        this.initForm();
    }

    public ngOnDestroy(): void {
        this.loginPresenter.destroy();
    }

    private initForm(): void {
        this.formLogin = this.formBuilder.group({
            email: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
            password: ['', Validators.required]
        });
    }


    public doLogin() {
        this.user.email = this.formLogin.controls.email?.value;
        this.user.password = this.formLogin.controls.password?.value;
        this.loginPresenter.doLogin(this.user);
    }

    public goToHome(user: User): void {
        const params: NavigationExtras = {
            state: {
                ...user
            }
        };
        this.router.navigate(['/home'], params);
    }

    public showLoading() {
        this.presentLoading();
    }

    public async hideLoading() {
        return await this.loadingController.dismiss();
    }

    public showError(error: string): void {
        this.presentToast(error);
    }


    async presentLoading() {
        const loading = await this.loadingController.create({
            message: 'Please wait...'
        });
        await loading.present();
    }

    async presentToast(message: string) {
        const toast = await this.toastController.create({
            message,
            duration: 2500
        });
        toast.present();
    }
}

