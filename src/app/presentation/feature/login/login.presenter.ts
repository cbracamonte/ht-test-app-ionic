import { Injectable } from '@angular/core';
import { Presenter } from '../../presenter';
import { LoadView } from '../../LoadView';
import { UserUseCase } from 'src/app/usecase/usecase/User/UserUseCase';
import { User } from 'src/app/entities/User';


@Injectable({
    providedIn: 'root'
})
export class LoginPresenter implements Presenter {

    private loginView: LoginView;
    private readonly userUseCase: UserUseCase;
    constructor(userUseCase: UserUseCase) {
        this.userUseCase = userUseCase;
    }

    setLoginView(loginView: LoginView) {
        this.loginView = loginView;
    }

    destroy(): void {
        this.loginView = null;
    }

    doLogin(user: User) {
        if (!this.loginView) {
            return;
        }
        this.loginView.showLoading();
        this.userUseCase.doLogin(user).subscribe(
            (userResponse: User) => {
                this.loginView.goToHome(userResponse);
            },
            ((error: Error) => {
                this.loginView.showError(error.message);
                this.loginView.hideLoading();
            }),
            () => {
                this.loginView.hideLoading();
            }
        );
    }
}

export interface LoginView extends LoadView {
    goToHome(user: User);
}
