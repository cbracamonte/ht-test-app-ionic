export class User {
    public email: string;
    public password: string;
    public name?: string;
    public token?: string;
    public lastLogin?: number;
    public created?: number;

    constructor() { }

    public getEmail(): string {
        return this.email;
    }

    public setEmail(email: string): void {
        this.email = email;
    }

    public getName(): string {
        return this.name;
    }

    public setName(name: string): void {
        this.name = name;
    }

    public getToken(): string {
        return this.token;
    }

    public setToken(token: string): void {
        this.token = token;
    }

    public getLastLogin(): number {
        return this.lastLogin;
    }

    public setLastLogin(lastLogin: number): void {
        this.lastLogin = lastLogin;
    }

    public getCreated(): number {
        return this.created;
    }

    public setCreated(created: number): void {
        this.created = created;
    }
}








/*
blUserLocale: "en"
created: 1594328027000
email: "carlosbracamonte@ht.pe"
lastLogin: 1594344865970
name: "carlos bracamonte"
objectId: "5453D375-7C99-433F-A4DB-FA942B86992D"
ownerId: "5453D375-7C99-433F-A4DB-FA942B86992D"
socialAccount: "BACKENDLESS"
updated: null
user-token: "485B28F3-B6E6-41A6-8D44-A60F52DB3722"
userStatus: "ENABLED"

*/