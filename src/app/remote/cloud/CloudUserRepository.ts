import { UserRepositoryRemote } from 'src/app/usecase/repository/User/UserRepositoryRemote';
import { User } from 'src/app/entities/User';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { UserRequest } from '../entity/entity-request/UserRequest.request';
import { map } from 'rxjs/operators';
import { UserEntity } from '../entity/UserEntity';
import { UserEntityResponse } from '../entity/entity-response/UserEntity.response';

@Injectable({
    providedIn: 'root'
})
export class CloudUserRepository implements UserRepositoryRemote {

    constructor() { }
    doLogin(user: User): Observable<User> {
        const userRequest: UserRequest = {
            email: user.email,
            password: user.password
        };
        const userObservable$ = this.toObservableUser(userRequest);
        return userObservable$.pipe(
            map((userEntity: UserEntity) => {
                return UserEntityResponse.toUser(userEntity);
            }));
    }

    toObservableUser(userRequest: UserRequest): Observable<UserEntity> {
        const observable$: Observable<UserEntity> = new Observable((observer) => {
            Backendless.UserService.login(userRequest.email, userRequest.password).then((response: any) => {
                observer.next(response);
            }).catch(error => {
                observer.error(error);
            }).finally(() => {
                observer.complete();
            });
        });

        return observable$;
    }
}




